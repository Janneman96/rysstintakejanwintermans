﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RYSSTIntakeJanWintermans.Models;

namespace RYSSTIntakeJanWintermans.Tests.DomainTests {
    [TestClass]
    public class ReservationValidationTests {
        //validateModel() tests
        [TestMethod]
        public void CanInitializeValidFullDayReservation() {
            //Arrange
            DateTime startTime = new DateTime(
                year: 2012,
                month: 3,
                day: 5,
                hour: Reservation.AVAILABLE_FROM_HOUR,
                minute: 0,
                second: 0);
            DateTime endTime = new DateTime(
                year: 2012,
                month: 3,
                day: 5,
                hour: Reservation.AVAILABLE_UNTIL_HOUR,
                minute: 0,
                second: 0);
            Reservation reservation = new Reservation(startTime, endTime);

            //Act
            var result = reservation.validateModel();

            //Assert
            Assert.AreEqual(0, result.Count);
        }

        [TestMethod]
        public void InvalidReservationReturnsAllModelErrors() {
            //Arrange
            DateTime startTime = new DateTime(
                year: 2012,
                month: 2,
                day: 4,
                hour: Reservation.AVAILABLE_UNTIL_HOUR + 1,
                minute: 1,
                second: 1);
            DateTime endTime = new DateTime(
                year: 2011,
                month: 3,
                day: 5,
                hour: Reservation.AVAILABLE_UNTIL_HOUR,
                minute: 15,
                second: 0);
            Reservation reservation = new Reservation(startTime, endTime);

            //Act
            var result = reservation.validateModel();

            //Assert
            Assert.AreEqual("The start time should be smaller than the end time.", result[0]);
            Assert.AreEqual("Rooms can only be rented between 7:00 and 17:00.", result[1]);
            Assert.AreEqual("Start date and end date are not on the same day.", result[2]);
            Assert.AreEqual("Rent hours should be in blocks of an hour. Use the same minutes for the start and end time.", result[3]);
            Assert.AreEqual("Seconds should be zero.", result[4]);
            Assert.AreEqual(5, result.Count);
        }

        //dateRangeIsValid() tests
        [TestMethod]
        public void StartTimeBiggerThanEndTimeReturnsError() {
            //Arrange
            DateTime startTime = new DateTime(
                year: 2012,
                month: 3,
                day: 5,
                hour: Reservation.AVAILABLE_FROM_HOUR + 1,
                minute: 15,
                second: 0);
            DateTime endTime = new DateTime(
                year: 2012,
                month: 3,
                day: 5,
                hour: Reservation.AVAILABLE_FROM_HOUR,
                minute: 15,
                second: 0);
            Reservation reservation = new Reservation(startTime, endTime);

            //Act
            var result = reservation.validateModel();

            //Assert
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("The start time should be smaller than the end time.", result[0]);
        }

        //dateRangeIsBetweenRentHours() tests
        [TestMethod]
        public void StartTimeIsBeforeAvailableStartTimeReturnsError() {
            //Arrange
            DateTime startTime = new DateTime(
                year: 2012,
                month: 3,
                day: 5,
                hour: Reservation.AVAILABLE_FROM_HOUR - 1,
                minute: 15,
                second: 0);
            DateTime endTime = new DateTime(
                year: 2012,
                month: 3,
                day: 5,
                hour: Reservation.AVAILABLE_FROM_HOUR + 1,
                minute: 15,
                second: 0);
            Reservation reservation = new Reservation(startTime, endTime);

            //Act
            var result = reservation.validateModel();

            //Assert
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("Rooms can only be rented between 7:00 and 17:00.", result[0]);
        }

        [TestMethod]
        public void EndTimeIsOneMinuteAfterAvailableEndTimeReturnsError() {
            //Arrange
            DateTime startTime = new DateTime(
                year: 2012,
                month: 3,
                day: 5,
                hour: Reservation.AVAILABLE_FROM_HOUR,
                minute: 1,
                second: 0);
            DateTime endTime = new DateTime(
                year: 2012,
                month: 3,
                day: 5,
                hour: Reservation.AVAILABLE_UNTIL_HOUR,
                minute: 1,
                second: 0);
            Reservation reservation = new Reservation(startTime, endTime);

            //Act
            var result = reservation.validateModel();

            //Assert
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("Rooms can only be rented between 7:00 and 17:00.", result[0]);
        }

        //datesAreOnSameDay() tests
        [TestMethod]
        public void StartAndEndAreNotOnTheSameDayReturnsError() {
            //Arrange
            DateTime startTime = new DateTime(
                year: 2012,
                month: 3,
                day: 5,
                hour: Reservation.AVAILABLE_FROM_HOUR,
                minute: 15,
                second: 0);
            DateTime endTime = new DateTime(
                year: 2012,
                month: 3,
                day: 6,
                hour: Reservation.AVAILABLE_FROM_HOUR + 1,
                minute: 15,
                second: 0);
            Reservation reservation = new Reservation(startTime, endTime);

            //Act
            var result = reservation.validateModel();

            //Assert
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("Start date and end date are not on the same day.", result[0]);
        }

        [TestMethod]
        public void StartAndEndAreNotInTheSameMonthReturnsError() {
            //Arrange
            DateTime startTime = new DateTime(
                year: 2012,
                month: 2,
                day: 6,
                hour: Reservation.AVAILABLE_FROM_HOUR,
                minute: 15,
                second: 0);
            DateTime endTime = new DateTime(
                year: 2012,
                month: 3,
                day: 6,
                hour: Reservation.AVAILABLE_FROM_HOUR + 1,
                minute: 15,
                second: 0);
            Reservation reservation = new Reservation(startTime, endTime);

            //Act
            var result = reservation.validateModel();

            //Assert
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("Start date and end date are not on the same day.", result[0]);
        }

        [TestMethod]
        public void StartAndEndAreNotInTheSameYearReturnsError() {
            //Arrange
            DateTime startTime = new DateTime(
                year: 2011,
                month: 3,
                day: 6,
                hour: Reservation.AVAILABLE_FROM_HOUR,
                minute: 15,
                second: 0);
            DateTime endTime = new DateTime(
                year: 2012,
                month: 3,
                day: 6,
                hour: Reservation.AVAILABLE_FROM_HOUR + 1,
                minute: 15,
                second: 0);
            Reservation reservation = new Reservation(startTime, endTime);

            //Act
            var result = reservation.validateModel();

            //Assert
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("Start date and end date are not on the same day.", result[0]);
        }

        //timeIsInHourBlocks() tests
        [TestMethod]
        public void EndMinutesIsOneMoreReturnsError() {
            //Arrange
            DateTime startTime = new DateTime(
                year: 2012,
                month: 3,
                day: 6,
                hour: Reservation.AVAILABLE_FROM_HOUR,
                minute: 15,
                second: 0);
            DateTime endTime = new DateTime(
                year: 2012,
                month: 3,
                day: 6,
                hour: Reservation.AVAILABLE_FROM_HOUR + 1,
                minute: 16,
                second: 0);
            Reservation reservation = new Reservation(startTime, endTime);

            //Act
            var result = reservation.validateModel();

            //Assert
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("Rent hours should be in blocks of an hour. Use the same minutes for the start and end time.", result[0]);
        }

        [TestMethod]
        public void EndMinutesIsOneLessReturnsError() {
            //Arrange
            DateTime startTime = new DateTime(
                year: 2012,
                month: 3,
                day: 6,
                hour: Reservation.AVAILABLE_FROM_HOUR,
                minute: 15,
                second: 0);
            DateTime endTime = new DateTime(
                year: 2012,
                month: 3,
                day: 6,
                hour: Reservation.AVAILABLE_FROM_HOUR + 1,
                minute: 14,
                second: 0);
            Reservation reservation = new Reservation(startTime, endTime);

            //Act
            var result = reservation.validateModel();

            //Assert
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("Rent hours should be in blocks of an hour. Use the same minutes for the start and end time.", result[0]);
        }

        //secondsAreZero() tests
        [TestMethod]
        public void StartTimeHasSecondsReturnsError() {
            //Arrange
            DateTime startTime = new DateTime(
                year: 2012,
                month: 3,
                day: 5,
                hour: 7,
                minute: 15,
                second: 1);
            DateTime endTime = new DateTime(
                year: 2012,
                month: 3,
                day: 5,
                hour: 9,
                minute: 15,
                second: 0);
            Reservation reservation = new Reservation(startTime, endTime);

            //Act
            var result = reservation.validateModel();

            //Assert
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("Seconds should be zero.", result[0]);
        }

        [TestMethod]
        public void StartTimeAndEndTimeHaveSecondsReturnsError() {
            //Arrange
            DateTime startTime = new DateTime(
                year: 2012,
                month: 3,
                day: 5,
                hour: 7,
                minute: 15,
                second: 1);
            DateTime endTime = new DateTime(
                year: 2012,
                month: 3,
                day: 5,
                hour: 9,
                minute: 15,
                second: 1);
            Reservation reservation = new Reservation(startTime, endTime);

            //Act
            var result = reservation.validateModel();

            //Assert
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("Seconds should be zero.", result[0]);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RYSSTIntakeJanWintermans.Models {
    public class Reservation {
        /// <summary>
        /// Start hour. AVAILABLE_FROM_HOUR:00 is included.
        /// </summary>
        public static int AVAILABLE_FROM_HOUR = 7;

        /// <summary>
        /// End hour. AVAILABLE_UNTIL_HOUR:01 is not included.
        /// </summary>
        public static int AVAILABLE_UNTIL_HOUR = 17;

        public int _id { get; set; }

        [Required]
        public DateTime StartTime { get; set; }

        [Required]
        public DateTime EndTime { get; set; }

        public Reservation(DateTime startTime, DateTime endTime) {
            this.StartTime = startTime;
            this.EndTime = endTime;
        }

        /// <summary>
        /// This list of strings should be added to the model errors in the controller.
        /// </summary>
        /// <returns>List of model validation error strings. Returns an empty list if there are no errors.</returns>
        public List<string> validateModel() {
            List<string> validationErrors = new List<string>();

            if (!dateRangeIsValid()) validationErrors.Add("The start time should be smaller than the end time.");
            if (!dateRangeIsBetweenRentHours()) validationErrors.Add("Rooms can only be rented between 7:00 and 17:00.");
            if (!datesAreOnSameDay()) validationErrors.Add("Start date and end date are not on the same day.");
            if (!timeIsInHourBlocks()) validationErrors.Add("Rent hours should be in blocks of an hour. Use the same minutes for the start and end time.");
            if (!secondsAreZero()) validationErrors.Add("Seconds should be zero.");

            return validationErrors;
        }

        private bool dateRangeIsValid() {
            return StartTime < EndTime;
        }

        private bool dateRangeIsBetweenRentHours() {
            if(EndTime.Hour == AVAILABLE_UNTIL_HOUR 
                && EndTime.Minute > 0) return false;

            return StartTime.Hour >= AVAILABLE_FROM_HOUR 
                && EndTime.Hour <= AVAILABLE_UNTIL_HOUR;
        }

        private bool datesAreOnSameDay() {
            return StartTime.Year == EndTime.Year
                && StartTime.Month == EndTime.Month
                && StartTime.Day == EndTime.Day;
        }

        private bool timeIsInHourBlocks() {
            //Decided the minutes should be the same 
            //    and having the startTime of one reservation being the same as the endTime of another Reservation is allowed.
            //This decision is better than forcing the user to use for example 7:15 - 8:14. Having 7:15 - 8:15 makes it easier to read and write for a human.
            return EndTime.Minute - StartTime.Minute == 0;
        }

        private bool secondsAreZero() {
            return StartTime.Second == 0 && EndTime.Second == 0;
        }
    }
}
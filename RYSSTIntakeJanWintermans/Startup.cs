﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(RYSSTIntakeJanWintermans.Startup))]
namespace RYSSTIntakeJanWintermans
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
